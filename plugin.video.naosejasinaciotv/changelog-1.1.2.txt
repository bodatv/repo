[1.1.2]
- Better player stream names

[1.1.1]
- Fix wrong list url

[1.1.0]
- Added play random stream setting
- Revised list formatting

[1.0.2]
- New list URL
- Added random query string to list url to avoid cache
- Other fixes

[1.0.1]
- Little fixes

[1.0.0]
- Initial release