import urllib, urllib2, re, gzip, socket, json, random, binascii
import xbmc, xbmcplugin, xbmcgui, xbmcaddon, sys, os
from urlparse import parse_qsl

# Get the plugin url in plugin:// notation.
_url = sys.argv[0]

# Get the plugin handle as an integer number.
_handle = int(sys.argv[1])

# Change default timeout for socket.
socket.setdefaulttimeout(5)

# Addon root dir.
addon = xbmcaddon.Addon(id='plugin.video.naosejasinaciotv')
rootDir = addon.getAddonInfo('path')
if rootDir[-1] == ';':
    rootDir = rootDir[0:-1]
rootDir = xbmc.translatePath(rootDir)

# Load the json channel feed.
list_request = urllib2.urlopen('http://tv.egg.pt/?'+binascii.b2a_hex(os.urandom(20))[:32])
LIST = json.load(list_request)

def list_channels():

    # Create a list for our items.
    listing = []

    # Channels.
    channels = LIST.keys()

    country_filter = get_country_filter()

    # Iterate through channels
    for channel in channels:

        if 'name' in LIST[channel]:

            if LIST[channel]['country'] in country_filter:

                # Channel country
                channel_country = LIST[channel]['country']
                if LIST[channel]['country'] == 'PT':
                    channel_country = '[COLOR green]PT:[/COLOR]'
                elif LIST[channel]['country'] == 'UK':
                    channel_country = '[COLOR blue]UK:[/COLOR]'

                # Channel count
                stream_count = len(get_filtered_links(channel))
                channel_count = '[COLOR orange]('+str(stream_count)+')[/COLOR]'

                # Channel label
                label = '[UPPERCASE][B]'+channel_country+' '+LIST[channel]['name']+' '+channel_count+'[/B][/UPPERCASE]'

                # List item
                list_item = xbmcgui.ListItem(label=label)

                # List item art
                list_item.setArt({
                    'thumb': LIST[channel]['icon'],
                    'fanart': LIST[channel]['fanart']
                })

                # Random play
                random_play = addon.getSetting('random_play')

                # URL
                url = '{0}?action=listing&channel={1}&name={2}&randomplay={3}'.format(_url, channel, LIST[channel]['name'], random_play)

                # Add channel to list
                listing.append((url, list_item, True))

    # Add our listing to Kodi.
    xbmcplugin.addDirectoryItems(_handle, listing, len(listing))

    # Add a sort method for the virtual folder items (alphabetically, ignore articles)
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)

    # Finish creating a virtual folder.
    xbmcplugin.endOfDirectory(_handle)


def list_channel_links(channel):

    # Create a list for our items.
    listing = []

    # Filter links
    links = get_filtered_links(channel)

    # Iterate through links.
    for link in links:

        # Channel quality
        if link['quality'] == '1080p':
            quality = '[B][COLOR green]1080p[/COLOR][/B]'
        elif link['quality'] == '720p':
            quality = '[B][COLOR orange]720p[/COLOR][/B]'
        else:
            quality = '[B][COLOR yellow]SD[/COLOR][/B]'

        # Channel user
        channel_user = ''
        if link['user']:
            channel_user = ' | '+link['user']

        # Channel label
        label = '[UPPERCASE]'+quality+' | Stream #'+link['number']+' | [B]'+link['server']+'[/B]'+channel_user+'[/UPPERCASE]'

        # Create list item
        list_item = xbmcgui.ListItem(label=label)
        list_item.setArt({
            'thumb': LIST[channel]['icon'],
            'fanart': LIST[channel]['fanart']
        })

        # List item info and properties
        list_item.setInfo('video', {'title': label})
        list_item.addStreamInfo('video', {'codec': 'h264'})

        # Play label
        play_label = '[UPPERCASE]'+link['quality']+' '+LIST[channel]['name']+' #'+link['number']+' ('+link['server']+')[/UPPERCASE]'
        url = '{0}?action=play&name={1}&url={2}'.format(_url, play_label, link['url'])

        # Add item to list
        listing.append((url, list_item, False))

    # Add our listing to Kodi.
    xbmcplugin.addDirectoryItems(_handle, listing, len(listing))

    # Add a sort method for the virtual folder items (alphabetically, ignore articles)
    xbmcplugin.addSortMethod(_handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)

    # Finish creating a virtual folder.
    xbmcplugin.endOfDirectory(_handle)


def get_country_filter():

    # Init.
    country_filter = []

    # Build country filter.
    if addon.getSetting('country_pt') == 'true':
        country_filter.append('PT')
    if addon.getSetting('country_uk') == 'true':
        country_filter.append('UK')

    return country_filter


def get_filtered_links(channel):

    # Init.
    links = LIST[channel]['links']
    quality_filter = []

    # Build quality filter.
    if addon.getSetting('quality_fhd') == 'true':
        quality_filter.append('1080p')
    if addon.getSetting('quality_hd') == 'true':
        quality_filter.append('720p')
    if addon.getSetting('quality_sd') == 'true':
        quality_filter.append('SD')
    
    if quality_filter:
        links = [d for d in links if d['quality'] in quality_filter]

    return links


def play(url, title):

    # Player icon.
    iconimage = xbmc.getInfoImage("ListItem.Thumb")

    # Set stream type.
    stream_type = ''
    if '.ts' in url:
        stream_type = 'TSDOWNLOADER'
    elif '.m3u8' in url:
        stream_type = 'HLSRETRY'
    if stream_type:
        from F4mProxy import f4mProxyHelper
        f4mp=f4mProxyHelper()
        f4mp.playF4mLink(url, title, proxy=None, use_proxy_for_chunks=True, maxbitrate=0, simpleDownloader=False, auth=None, streamtype=stream_type, setResolved=False, swf=None, callbackpath="", callbackparam="", iconImage=iconimage)
        return

    return

def router(paramstring):

    # Parse a URL-encoded paramstring to the dictionary of
    # {<parameter>: <value>} elements
    params = dict(parse_qsl(paramstring))

    # Check the parameters passed to the plugin
    if params:

        # Display channel links.
        if params['action'] == 'listing':

            if params['randomplay'] == 'false':
                list_channel_links(params['channel'])
            else:
                # get channel random stream
                stream = random.choice(get_filtered_links(params['channel']))

                # stream label
                stream_label = '[UPPERCASE]'+stream['quality']+' '+params['name']+' #'+stream['number']+' ('+stream['server']+')[/UPPERCASE]'
                play(stream['url'], stream_label)

        # Play a link
        elif params['action'] == 'play':
            play(params['url'], params['name'])
    else:
        # If the plugin is called from Kodi UI without any parameters,
        # display the list of video categories
        list_channels()


if __name__ == '__main__':
    # Call the router function and pass the plugin call parameters to it.
    # We use string slicing to trim the leading '?' from the plugin call paramstring
    router(sys.argv[2][1:])
